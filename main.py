#pip freeze > requirements.txt
#pip install -r requirements.txt

import os
import scraper
import parser
import csv
import json


def main():
    startup_urls = []
    if os.path.isfile('startup_urls.csv'):
        with open('startup_urls.csv', 'r') as f_in:
            reader = csv.reader(f_in)
            for row in reader:
                startup_urls = row
    else:
        startup_urls = scraper.scrape_startup_urls()
        with open('startup_urls.csv', 'w+') as f_out:
            writer = csv.writer(f_out)
            writer.writerow(startup_urls)

    startups = []
    for i, startup_url in enumerate(startup_urls):
        print("Startup number: " + str(i))
        print(startup_url)
        startup_html = scraper.scrape_url(startup_url)
        startup = parser.parse_page_content(startup_html)
        startups.append(startup)
    with open('startups.txt', 'w+') as f_out:
        json.dump(startups, f_out)


if __name__ == "__main__":
    main()
